package ru.kuzminykh.tm.repository;

import ru.kuzminykh.tm.entity.Project;
import ru.kuzminykh.tm.entity.Task;

import java.util.*;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    private final HashMap<String,List<Project>> indexProjects = new HashMap<>();

    public Project addProjectToHashMap(final Project project){
        List<Project> projectsIdx = indexProjects.get(project.getName());
        if (projectsIdx == null) projectsIdx = new  ArrayList<>();
        projectsIdx.add(project);
        indexProjects.put(project.getName(),projectsIdx);
        return project;
    }

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        addProjectToHashMap(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        addProjectToHashMap(project);
        return project;
    }

    public Project create(final String name, final String description, Long userId) {
        final Project project = create(name, description);
        project.setUserId(userId);
        addProjectToHashMap(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        String oldName = project.getName();
        List<Project> projectList = findByName(oldName);
        indexProjects.remove(project.getName());
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        indexProjects.put(name,projectList);
        return project;
    }

    public void clear() {
        projects.clear();
        indexProjects.clear();
    }

    public Project findByIndex(final int index) {
        if (index < 0 || index > projects.size() - 1) return null;
        return projects.get(index);
    }

    public List<Project> findByName(final String name) {
        if(indexProjects.get(name) == null) return null;
        return new ArrayList<>(indexProjects.get(name));
    }

    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project removeProject(Project project) {
        final List<Project> projectname = findByName(project.getName());
        if (projectname == null || projectname.size() == 0) return null;
        projects.removeAll(projectname);
        indexProjects.remove(project.getName());
        return project;
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        removeProject(project);
        return project;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        removeProject(project);
        return project;
    }

    public List<Project> removeByName(final String name) {
        List<Project> project = findByName(name);
        if (project == null || project.size() == 0) return null;
        projects.removeAll(project);
        indexProjects.remove(name);
        return project;
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : findAll()) {
            final Long IdUser = project.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(project);
        }
        return result;
    }

    public List<Project> findAll() {
        return projects;
    }

}
