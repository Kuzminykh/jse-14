package ru.kuzminykh.tm.repository;

import ru.kuzminykh.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    private final HashMap<String,List<Task>> indexTasks = new HashMap<>();

    public Task addTaskToHashMap(final Task task){
        List<Task> tasksIdx = indexTasks.get(task.getName());
        if (tasksIdx == null) tasksIdx = new  ArrayList<>();
        tasksIdx.add(task);
        indexTasks.put(task.getName(),tasksIdx);
        return task;
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        addTaskToHashMap(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        addTaskToHashMap(task);
        return task;
    }

    public Task create(final String name, final String description, Long userId) {
        final Task task = create(name, description);
        task.setUserId(userId);
        addTaskToHashMap(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        String oldName = task.getName();
        List<Task> taskList = findByName(oldName);
        if(taskList == null) return null;
        indexTasks.remove(task.getName());
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        indexTasks.put(name,taskList);
        return task;
    }

    public void clear() {
        tasks.clear();
        indexTasks.clear();
    }

    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() - 1) return null;
        return tasks.get(index);
    }

    public List<Task> findByName(final String name) {
        if(indexTasks.get(name) == null) return null;
        return new ArrayList<>(indexTasks.get(name));
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task: tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeTask(Task task) {
        final List<Task> taskname = findByName(task.getName());
        if (taskname == null || taskname.size() == 0) return null;
        tasks.removeAll(taskname);
        indexTasks.remove(task.getName());
        return task;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        removeTask(task);
        return task;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        removeTask(task);
        return task;
    }

    public List<Task> removeByName(final String name) {
        final List<Task> task = findByName(name);
        if (task == null || task.size() == 0) return null;
        tasks.removeAll(task);
        indexTasks.remove(name);
        return task;
    }

    public List<Task> findAllByProjectId(final Long projectId){
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()){
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long IdUser = task.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAll() {
        return tasks;
    }

}
